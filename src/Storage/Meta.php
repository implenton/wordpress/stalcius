<?php

namespace Stalcius\Storage;

class Meta extends Base {
    protected $type = 'post';

    public function __construct( $config ) {
        parent::__construct( $config );

        if ( $this->is_type_set( $config ) ) {
            $this->type = $config['meta'];
        }
    }

    public function save( $location, $list ) {
        $meta = $this->normalize_meta( $location );

        $meta_type = $this->get_type();
        $item_id   = $meta['item_id'];
        $meta_key  = $meta['key'];

        return update_metadata( $meta_type, $item_id, $meta_key, $list );
    }

    public function get( $location ) {
        $meta = $this->normalize_meta( $location );

        $meta_type = $this->get_type();
        $item_id   = $meta['item_id'];
        $meta_key  = $meta['key'];

        return get_metadata( $meta_type, $item_id, $meta_key, true );
    }

    public function delete( $location ) {
        $meta = $this->normalize_meta( $location );

        $meta_type = $this->get_type();
        $item_id   = $meta['item_id'];
        $meta_key  = $meta['key'];

        return delete_metadata( $meta_type, $item_id, $meta_key );
    }

    protected function is_type_set( $config ) {
        return isset( $config['meta'] ) && ! empty( $config['meta'] ) ? true : false;
    }

    protected function get_type() {
        return $this->type;
    }

    protected function normalize_meta( $location ) {
        return array_combine( [ 'key', 'item_id' ], $location );
    }
}
