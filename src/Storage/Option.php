<?php

namespace Stalcius\Storage;

class Option extends Base {
    public function save( $location, $list ) {
        $key = $this->normalize_name( $location );

        return update_option( $key, $list );
    }

    public function get( $location ) {
        $key = $this->normalize_name( $location );

        return get_option( $key, '' );
    }

    public function delete( $location ) {
        $key = $this->normalize_name( $location );

        return delete_option( $key );
    }

    protected function normalize_name( $location ) {
        return is_array( $location ) ? implode( '_', $location ) : $location;
    }
}
