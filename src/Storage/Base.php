<?php

namespace Stalcius\Storage;

abstract class Base {
    protected $config;

    public function __construct( $config ) {
        $this->config = $config;
    }

    abstract public function save( $location, $list );

    abstract public function get( $location );

    abstract public function delete( $location );
}
