<?php

namespace Stalcius\Strategy;

abstract class Base {
    protected $config;

    public function __construct( $config ) {
        $this->config = $config;
    }

    abstract public function add_item( $list, $item );

    abstract public function remove_item( $list, $item );

    abstract public function update_item( $list, $item );

    abstract public function has_item( $list, $item );

    abstract public function get_item( $list, $item );

    public function get_all_items( $list ) {
        return $this->get_storage( $list );
    }

    public function delete_all_items( $list ) {
        return $this->delete_storage( $list );
    }

    public function is_empty_list( $list ) {
        return empty( $this->get_storage( $list ) );
    }

    public function has_any_item( $list ) {
        return ! $this->is_empty_list( $list );
    }

    public function toggle_item( $list, $item ) {
        return $this->has_item( $list, $item ) ? $this->remove_item( $list, $item ) : $this->add_item( $list, $item );
    }

    protected function get_storage( $location ) {
        $storage = $this->init_storage_handler();

        return call_user_func( [ $storage, 'get' ], $location );
    }

    protected function save_storage( $location, $list_of_items ) {
        $storage = $this->init_storage_handler();

        return call_user_func( [ $storage, 'save' ], $location, $list_of_items );
    }

    protected function delete_storage( $location ) {
        $storage = $this->init_storage_handler();

        return call_user_func( [ $storage, 'delete' ], $location );
    }

    protected function init_storage_handler() {
        $storageClass = $this->config['storage_handler'];

        return new $storageClass( $this->config );
    }
}
