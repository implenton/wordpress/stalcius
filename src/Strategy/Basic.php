<?php

namespace Stalcius\Strategy;

class Basic extends Base {
    public function add_item( $list, $item ) {
        if ( ! $this->is_valid_item( $item ) ) {
            return false;
        }

        if ( $this->has_item( $list, $item ) ) {
            return false;
        }

        $current_items = $this->get_all_items( $list );
        $new_list      = array_merge( $current_items, [ $item ] );

        return $this->save_storage( $list, $new_list );
    }

    public function remove_item( $list, $item ) {
        if ( ! $this->is_valid_item( $item ) ) {
            return false;
        }

        if ( ! $this->has_item( $list, $item ) ) {
            return false;
        }

        $current_list = $this->get_all_items( $list );
        $new_list     = array_diff( $current_list, [ $item ] );


        return empty( $new_list ) ? $this->delete_storage( $list ) : $this->save_storage( $list, $new_list );
    }

    public function update_item( $list, $item ) {
        return false;
    }

    public function has_item( $list, $item ) {
        $list = $this->get_all_items( $list );

        return in_array( $item, $list, true );
    }

    public function get_item( $list, $item ) {
        return $item;
    }

    public function get_all_items( $list ) {
        $items = $this->get_storage( $list );

        return empty( $items ) ? [] : $items;
    }

    protected function is_valid_item( $item ) {
        return is_array( $item ) || is_object( $item ) || is_bool( $item ) || empty( $item ) ? false : true;
    }
}
