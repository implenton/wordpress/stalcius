<?php

namespace Stalcius;

class Stalcius {
    private static $instance = null;
    private $list = [];

    public static function get_instance() {
        if ( null === self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function set_list( $list_name, $base_config ) {
        if ( ! ( $config = $this->set_list_config( $list_name, $base_config ) ) ) {
            return false;
        }

        return $this->list[ $list_name ] = $config;
    }

    public function get_list() {
        return $this->list;
    }

    public function get_list_config( $name ) {
        return isset( $this->list[ $name ] ) ? $this->list[ $name ] : null;
    }

    private function set_list_config( $list_name, $base_config ) {
        if ( ! isset( $base_config['strategy'] ) ) {
            // TODO: maybe add _doing_it_wrong
            return false;
        }

        if ( ! isset( $base_config['storage'] ) ) {
            return false;
        }

        if ( is_null( $strategy_handler = $this->get_strategy_handler( $base_config['strategy'] ) ) ) {
            return false;
        }

        if ( is_null( $storage_handler = $this->get_storage_handler( $base_config['storage'] ) ) ) {
            return false;
        }

        $config['name']             = $list_name;
        $config['strategy_handler'] = $strategy_handler;
        $config['storage_handler']  = $storage_handler;

        return wp_parse_args( $base_config, $config );
    }

    public static function dispatch_action( $list, $action, ...$vars ) {
        $stalcius    = self::get_instance();
        $list_name   = is_array( $list ) ? $list[0] : $list;
        $list_config = $stalcius->get_list_config( $list_name );

        if ( is_null( $list_config ) ) {
            return null;
        }

        $strategy_handler = $list_config['strategy_handler'];
        $handler          = new $strategy_handler( $list_config );

        return call_user_func_array( [ $handler, $action ], array_merge( [ $list ], $vars ) );
    }

    private function get_strategy_handler( $key ) {
        $mapList = apply_filters( 'stalcius/strategies', [
            'basic' => Strategy\Basic::class,
        ] );

        return $this->is_valid_handler( $mapList, $key ) ? $mapList[ $key ] : null;
    }

    private function get_storage_handler( $key ) {
        $mapList = apply_filters( 'stalcius/storages', [
            'meta'   => Storage\Meta::class,
            'option' => Storage\Option::class,
            'file'   => Storage\File::class,
        ] );

        return $this->is_valid_handler( $mapList, $key ) ? $mapList[ $key ] : null;
    }

    private function is_valid_handler( $map, $label ) {
        return isset( $map[ $label ] ) && class_exists( $map[ $label ] ) ? true : false;
    }
}