<?php
/*
 * Plugin Name: Stalcius
 * Description: Manage lists.
 * Plugin URI:  https://implenton.com
 * Version:     0.1.0
 * Author:      implenton
 * Author URI:  https://implenton.com
 * License:     GPLv3
 * License URI: LICENSE
 */

use Stalcius\Stalcius;

define( 'STALCIUS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'STALCIUS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once( STALCIUS_PLUGIN_PATH . '/src/Stalcius.php' );

require_once( STALCIUS_PLUGIN_PATH . '/src/Strategy/Base.php' );
require_once( STALCIUS_PLUGIN_PATH . '/src/Strategy/Basic.php' );

require_once( STALCIUS_PLUGIN_PATH . '/src/Storage/Base.php' );
require_once( STALCIUS_PLUGIN_PATH . '/src/Storage/Meta.php' );
require_once( STALCIUS_PLUGIN_PATH . '/src/Storage/Option.php' );

function stalcius_register_list( $list, $config ) {
    $stalcius = Stalcius::get_instance();
    $stalcius->set_list( $list, $config );

    return $stalcius->get_list();
}

function stalcius_get_registered_lists() {
    $stalcius = Stalcius::get_instance();

    return $stalcius->get_list();
}

function stalcius_add_item( $list, $item ) {
    return Stalcius::dispatch_action( $list, 'add_item', $item );
}

function stalcius_remove_item( $list, $item ) {
    return Stalcius::dispatch_action( $list, 'remove_item', $item );
}

function stalcius_update_item( $list, $item ) {
    return Stalcius::dispatch_action( $list, 'update_item', $item );
}

function stalcius_has_item( $list, $item ) {
    return Stalcius::dispatch_action( $list, 'has_item', $item );
}

function stalcius_get_item( $list, $item ) {
    return Stalcius::dispatch_action( $list, 'get_item', $item );
}

function stalcius_get_all_items( $list ) {
    return Stalcius::dispatch_action( $list, 'get_list' );
}

function stalcius_is_empty_list( $list ) {
    return Stalcius::dispatch_action( $list, 'is_empty_list' );
}

function stalcius_has_any_item( $list ) {
    return Stalcius::dispatch_action( $list, 'has_any_item' );
}

function stalcius_toggle_item( $list, $item ) {
    return Stalcius::dispatch_action( $list, 'toggle_item', $item );
}

function stalcius_delete_all_items( $list ) {
    return Stalcius::dispatch_action( $list, 'delete_all_items' );
}